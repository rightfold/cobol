       identification division.
       program-id. loser.

       data division.
       working-storage section.
           copy "is-loser.copy".

       procedure division.
       main-para.
           move "anybody not rightfold" to name
           perform determine-loserness-para

           move "rightfold" to name
           perform determine-loserness-para

           goback
           .

       determine-loserness-para.
           call "is-loser" using by content name, by reference result
           if is-awesome then
               display name, " is awesome"
           else
               display name, " is a loser"
           end-if
           .
