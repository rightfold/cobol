       identification division.
       program-id. fizzbuzz.

       data division.
       working-storage section.
           01 n                        pic 9(3).

       procedure division.
           perform 100 times
               add 1 to n
               if 0 = function mod(n, 3) or function mod(n, 5) then
                   if function mod(n, 3) = 0 then
                       display "fizz" with no advancing
                   end-if
                   if function mod(n, 5) = 0 then
                       display "buzz" with no advancing
                   end-if
               else
                   display n with no advancing
               end-if
               display " "
           end-perform
           .
