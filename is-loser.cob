       identification division.
       program-id. is-loser.

       data division.
       linkage section.
           copy "is-loser.copy".

       procedure division using name, result.
           if name = "rightfold" then
               set is-awesome to true
           else
               set is-loser to true
           end-if
           goback
           .
